#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Set mask in CXI file
Author: Sergey Bobkov
"""

import os
import sys
import shutil
import subprocess
import argparse
import h5py
import numpy as np
from tqdm import tqdm

from spi_processing import cxidata


def set_mask(fname: str, mask_data: np.ndarray, dset_name='mask', chunk_size=100):
    image_ids = cxidata.get_image_groups(fname)

    for image_id in image_ids:
        data_shape = cxidata.get_dataset_shape(fname, image_id, 'data')

        if data_shape[1:] != mask_data.shape:
            raise ValueError('Mask shape {} do not with with the data shape {} in image {}'.format(mask_data.shape, data_shape, image_id))

        for start in range(0, data_shape[0], chunk_size):
            end = min(start+chunk_size, data_shape[0])
        
            data = cxidata.read_dataset(fname, image_id, 'data', start, end)
            data[:, mask_data != 0] = 0
            cxidata.update_dataset(fname, image_id, 'data', data, start, end)

        if dset_name in cxidata.get_names(fname, image_id):
            cxidata.delete_dataset(fname, image_id, dset_name)

        cxidata.save_dataset(fname, image_id, dset_name, mask_data)


def main():
    parser = argparse.ArgumentParser(description='Set mask CXI data')
    parser.add_argument('files', metavar='FILE', nargs='+', help='Input files')
    parser.add_argument('-o', '--out', dest='output_dir', help="Output directory")
    parser.add_argument('-m', '--maskfile', metavar='FILE', required=True, help="HDF5 file with mask array")
    parser.add_argument('-d', '--dataset', metavar='PATH', default='/mask', help="HDF5 path for mask array")
    parser_args = parser.parse_args()

    input_files = parser_args.files
    output_dir = parser_args.output_dir
    maskfile = parser_args.maskfile
    dataset_path = parser_args.dataset

    for fname in input_files:
        if not os.path.isfile(fname):
            parser.error("File {} doesn't exist".format(fname))
    
    if not os.path.isfile(maskfile):
        parser.error("File {} doesn't exist".format(maskfile))

    with h5py.File(maskfile, 'r') as h5file:
        if dataset_path not in h5file:
            parser.error("Dataset {} doesn't exist in file {}".format(dataset_path, maskfile))

        mask_data = h5file[dataset_path][:]

    if output_dir:
        os.makedirs(output_dir, exist_ok=True)
        sys.stderr.write("Copying to output\n")
        os.makedirs(output_dir, exist_ok=True)
        new_input_files = []
        for fname in tqdm(input_files):
            new_fname = os.path.join(output_dir, os.path.basename(fname))
            shutil.copy(fname, new_fname)
            subprocess.call(["/bin/chmod", "u+w", new_fname])
            new_input_files.append(new_fname)
        input_files = new_input_files

    for fname in tqdm(input_files):
        set_mask(fname, mask_data)


if __name__ == '__main__':
    main()
