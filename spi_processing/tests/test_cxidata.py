"""
Tests for cxidata module
Author: Sergey Bobkov
"""

import pytest
import numpy as np
import h5py

from spi_processing import cxidata


def test_create(tmpdir):
    filename = tmpdir.join('tmpfile.h5')
    cxidata.create_file(filename, rewrite=True)

    with h5py.File(filename, 'r') as h5file:
        assert 'entry_1' in h5file
        assert 'cxi_version' in h5file

    with pytest.raises(FileExistsError):
        cxidata.create_file(filename)


def test_image(tmpdir):
    filename = tmpdir.join('tmpfile.h5')
    cxidata.create_file(filename)

    with pytest.raises(FileNotFoundError):
        cxidata.add_image_group(filename+'tmp')

    ids = [cxidata.add_image_group(filename) for i in range(5)]

    get_ids = cxidata.get_image_groups(filename)

    for i in range(5):
        assert i+1 in ids
        assert i+1 in get_ids


def test_dataset_names(tmpdir):
    filename = tmpdir.join('tmpfile.h5')
    cxidata.create_file(filename)

    data = np.random.randint(1, 100, size=(10, 5))

    image_id = cxidata.add_image_group(filename)

    names = ["path/to/file", "path/to/another/file", "some/data", "instrument_1/data",
             "some/dataset", "something"]

    for n in names:
        cxidata.save_dataset(filename, image_id, n, data)

    read_names = cxidata.get_names(filename, image_id)

    assert len(names) == len(read_names)

    for n in names:
        assert n in read_names


def test_dataset_create_delete(tmpdir):
    filename = tmpdir.join('tmpfile.h5')
    cxidata.create_file(filename)

    data = np.random.randint(1, 100, size=(10, 5))

    image_id = cxidata.add_image_group(filename)

    name = "path/to/file"

    cxidata.create_dataset(filename, image_id, name, data.shape, data.dtype)
    assert name in cxidata.get_names(filename, image_id)

    cxidata.update_dataset(filename, image_id, name, data)
    read_data = cxidata.read_dataset(filename, image_id, name)
    assert (data == read_data).all()

    cxidata.delete_dataset(filename, image_id, name)
    assert name not in cxidata.get_names(filename, image_id)


def test_dataset_chunks_compression(tmpdir):
    filename = tmpdir.join('tmpfile.h5')
    cxidata.create_file(filename)

    data = np.random.randint(1, 100, size=(10, 5))

    image_id = cxidata.add_image_group(filename)

    name = 'path/to/data'

    cxidata.save_dataset(filename, image_id, name, data, chunks=(1,2), compression="lzf")
    assert cxidata.get_dataset_chunks(filename, image_id, name) == (1,2)
    assert cxidata.get_dataset_compression(filename, image_id, name) == "lzf"

    name = 'path/to/data2'

    cxidata.save_dataset(filename, image_id, name, data, chunks=(5,1), compression="gzip")
    assert cxidata.get_dataset_chunks(filename, image_id, name) == (5,1)
    assert cxidata.get_dataset_compression(filename, image_id, name) == "gzip"


def test_read_write_dset(tmpdir):
    filename = tmpdir.join('tmpfile.h5')
    cxidata.create_file(filename)

    image_id = cxidata.add_image_group(filename)

    data = np.random.randint(1, 100, size=(20, 20))

    name = "path/to/file"

    cxidata.save_dataset(filename, image_id, name, data)
    read_data = cxidata.read_dataset(filename, image_id, name)
    assert (data == read_data).all()
    read_data_shape = cxidata.get_dataset_shape(filename, image_id, name)
    assert read_data_shape == data.shape

    data_chunk = cxidata.read_dataset(filename, image_id, name, 12, 20)
    assert (data[12:20] == data_chunk).all()

    with pytest.raises(ValueError):
        cxidata.read_dataset(filename, image_id, name, 2, 21)

    with pytest.raises(ValueError):
        cxidata.save_dataset(filename, image_id, name, data)

    with pytest.raises(ValueError):
        cxidata.read_dataset(filename, image_id, name, -10)

    with pytest.raises(ValueError):
        cxidata.read_dataset(filename, image_id, name, chunk_end=-10)

    with pytest.raises(ValueError):
        cxidata.read_dataset(filename, image_id, name, 12, 8)


def test_update_chunk(tmpdir):
    filename = tmpdir.join('tmpfile.h5')
    cxidata.create_file(filename)

    image_id = cxidata.add_image_group(filename)

    data = np.random.randint(1, 100, size=(20, 20))
    name = "path/to/file"

    cxidata.save_dataset(filename, image_id, name, data)

    chunk_data = cxidata.read_dataset(filename, image_id, name, 5, 7)
    cxidata.update_dataset(filename, image_id, name, chunk_data + 10, 5, 7)
    data[5:7] += 10

    read_data = cxidata.read_dataset(filename, image_id, name)
    assert (data == read_data).all()

    with pytest.raises(ValueError):
        cxidata.update_dataset(filename, image_id, name, chunk_data + 10, 4, 8)


def test_dataset_sum(tmpdir):
    filename = tmpdir.join('tmpfile.h5')
    cxidata.create_file(filename)

    image_id = cxidata.add_image_group(filename)

    data = np.random.randint(1, 100, size=(20, 20, 20))
    name = "path/to/file"

    cxidata.save_dataset(filename, image_id, name, data)

    for axis in [None, 0, 1, 2, (0, 1), (1, 2), (0, 2)]:
        assert (data.sum(axis=axis) == cxidata.compute_dataset_sum(filename, image_id, name, axis=axis)).all()
