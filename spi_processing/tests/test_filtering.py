"""
Tests for cxidata module
Author: Sergey Bobkov
"""

import numpy as np

from spi_processing import cxidata, filtering


def test_filter_files(tmpdir):
    filename1 = tmpdir.join('tmpfile1.h5')
    filename2 = tmpdir.join('tmpfile2.h5')

    cxidata.create_file(filename1)

    image_id = cxidata.add_image_group(filename1)

    data = np.random.randint(1, 100, size=(20, 20))
    data2 = np.random.randint(1, 100, size=(400, 20, 20))
    data_filter = np.random.randint(1, 100, size=(400,))

    cxidata.save_dataset(filename1, image_id, 'data', data)
    cxidata.save_dataset(filename1, image_id, 'data2', data2)
    cxidata.save_dataset(filename1, image_id, 'data_filter', data_filter)

    sel = np.logical_and(data_filter >= 30, data_filter <= 60)
    filtering.filter_file(filename1, filename2, 'data_filter', 30, 60)

    read_data = cxidata.read_dataset(filename2, image_id, 'data')
    read_data2 = cxidata.read_dataset(filename2, image_id, 'data2')
    read_data_filter = cxidata.read_dataset(filename2, image_id, 'data_filter')

    assert (read_data == data).all()
    assert (read_data2 == data2[sel]).all()
    assert (read_data_filter == data_filter[sel]).all()
