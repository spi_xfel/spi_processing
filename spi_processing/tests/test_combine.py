"""
Tests for cxidata module
Author: Sergey Bobkov
"""

import os
import pytest
import numpy as np

from spi_processing import cxidata, combine


def test_combine_files(tmpdir):
    filename1 = tmpdir.join('tmpfile1.h5')
    filename2 = tmpdir.join('tmpfile2.h5')
    filename_combined = tmpdir.join('tmpfile_combined.h5')

    cxidata.create_file(filename1)
    cxidata.create_file(filename2)

    image_id = cxidata.add_image_group(filename1)
    image_id = cxidata.add_image_group(filename2)

    mask1 = np.random.randint(1, 100, size=(20, 20))
    image_center1 = np.random.randint(1, 100, size=(3,))
    data = np.random.randint(1, 100, size=(20, 20))

    name = 'data'
    cxidata.save_dataset(filename1, image_id, name, data[:10])
    cxidata.save_dataset(filename2, image_id, name, data[10:])

    name = 'mask'
    cxidata.save_dataset(filename1, image_id, name, mask1)
    cxidata.save_dataset(filename2, image_id, name, mask1)

    name = 'image_center'
    cxidata.save_dataset(filename1, image_id, name, image_center1)
    cxidata.save_dataset(filename2, image_id, name, image_center1)

    name = 'name'
    names = [n.encode('utf8') for n in ["John", "Mike"]]
    cxidata.save_dataset(filename1, image_id, name, names[0])
    cxidata.save_dataset(filename2, image_id, name, names[1])

    name = 'name2'
    cxidata.save_dataset(filename1, image_id, name, names[0])
    cxidata.save_dataset(filename2, image_id, name, names[0])

    combine.combine_files([filename1, filename2], filename_combined)

    assert len(cxidata.get_image_groups(filename_combined)) == 1
    assert (cxidata.read_dataset(filename_combined, image_id, 'mask') == mask1).all()
    assert (cxidata.read_dataset(filename_combined, image_id, 'image_center') == image_center1).all()
    assert (cxidata.read_dataset(filename_combined, image_id, 'data') == data).all()
    assert (cxidata.read_dataset(filename_combined, image_id, 'name') == names).all()
    assert cxidata.read_dataset(filename_combined, image_id, 'name2') == names[0]


def test_combine_files2(tmpdir):
    filename1 = tmpdir.join('tmpfile1.h5')
    filename2 = tmpdir.join('tmpfile2.h5')
    filename_combined = tmpdir.join('tmpfile_combined.h5')

    cxidata.create_file(filename1)
    cxidata.create_file(filename2)

    image_id = cxidata.add_image_group(filename1)
    image_id = cxidata.add_image_group(filename2)

    mask1 = np.random.randint(1, 100, size=(20, 20))
    mask2 = np.random.randint(1, 100, size=(20, 20))
    image_center1 = np.random.randint(1, 100, size=(3,))
    image_center2 = np.random.randint(1, 100, size=(3,))

    name = 'mask'
    cxidata.save_dataset(filename1, image_id, name, mask1)
    cxidata.save_dataset(filename2, image_id, name, mask2)

    name = 'image_center'
    cxidata.save_dataset(filename1, image_id, name, image_center1)
    cxidata.save_dataset(filename2, image_id, name, image_center1)

    combine.combine_files([filename1, filename2], filename_combined)
    assert len(cxidata.get_image_groups(filename_combined)) == 2

    name = 'image_center'
    cxidata.update_dataset(filename2, image_id, name, image_center2)

    with pytest.raises(FileExistsError):
        combine.combine_files([filename1, filename2], filename_combined)

    os.remove(filename_combined)
    combine.combine_files([filename1, filename2], filename_combined)
    assert len(cxidata.get_image_groups(filename_combined)) == 2

    name = 'mask'
    cxidata.update_dataset(filename1, image_id, name, mask2)

    os.remove(filename_combined)
    combine.combine_files([filename1, filename2], filename_combined)
    assert len(cxidata.get_image_groups(filename_combined)) == 2

    name = 'image_center'
    cxidata.update_dataset(filename1, image_id, name, image_center2)

    os.remove(filename_combined)
    combine.combine_files([filename1, filename2], filename_combined)
    assert len(cxidata.get_image_groups(filename_combined)) == 1

def test_combine_files3(tmpdir):
    filename1 = tmpdir.join('tmpfile1.h5')
    filename2 = tmpdir.join('tmpfile2.h5')
    filename_combined = tmpdir.join('tmpfile_combined.h5')

    cxidata.create_file(filename1)
    cxidata.create_file(filename2)

    image_id = cxidata.add_image_group(filename1)
    image_id = cxidata.add_image_group(filename2)

    mask1 = np.random.randint(1, 100, size=(20, 20))
    image_center1 = np.random.randint(1, 100, size=(3,))

    cxidata.save_dataset(filename1, image_id, 'mask', mask1)
    cxidata.save_dataset(filename1, image_id, 'image_center', image_center1)

    combine.combine_files([filename1, filename2], filename_combined)
    assert len(cxidata.get_image_groups(filename_combined)) == 2

    cxidata.save_dataset(filename2, image_id, 'mask', mask1)
    cxidata.save_dataset(filename2, image_id, 'image_center', image_center1)

    os.remove(filename_combined)
    combine.combine_files([filename1, filename2], filename_combined)
    assert len(cxidata.get_image_groups(filename_combined)) == 1

    cxidata.delete_dataset(filename1, image_id, 'mask')
    cxidata.delete_dataset(filename1, image_id, 'image_center')

    os.remove(filename_combined)
    combine.combine_files([filename1, filename2], filename_combined)
    assert len(cxidata.get_image_groups(filename_combined)) == 2

    cxidata.delete_dataset(filename2, image_id, 'mask')
    cxidata.delete_dataset(filename2, image_id, 'image_center')

    os.remove(filename_combined)
    combine.combine_files([filename1, filename2], filename_combined)
    assert len(cxidata.get_image_groups(filename_combined)) == 1
