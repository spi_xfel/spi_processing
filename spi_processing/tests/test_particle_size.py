"""
Tests for cxidata module
Author: Sergey Bobkov
"""

import numpy as np

from spi_processing import particle_size, sphere, cxidata


def test_size(tmpdir):
    filename = tmpdir.join('tmpfile.h5')
    cxidata.create_file(filename)

    image_id = cxidata.add_image_group(filename)

    size = sphere.compute_size(10, 1)


    num_frames = 10
    shape = (40,40)
    center = np.array([20,20,0])
    sizes = (np.random.random(num_frames) + 0.5)*size
    sizes = np.round(sizes, decimals=2)
    data = np.zeros((num_frames, )+ shape)

    for i in range(num_frames):
        data[i] = sphere.simulate_pattern(shape, center, sizes[i]/2, 1, 1)

    rand = np.random.randint(1, 100, size=shape)
    mask = (rand > 90)
    data[:, mask != 0] = 0

    cxidata.save_dataset(filename, image_id, 'data', data)
    cxidata.save_dataset(filename, image_id, 'mask', mask)
    cxidata.save_dataset(filename, image_id, 'image_center', center)

    particle_size.compute_psd_data(filename)
    particle_size.estimate_size(filename, 1, 1, 1, size_min=size/10, size_max=size*10)

    result_sizes = cxidata.read_dataset(filename, image_id, 'psd/size')
    result_sizes = np.round(result_sizes, decimals=2)

    assert (sizes == result_sizes).all()
