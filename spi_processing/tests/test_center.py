"""
Tests for cxidata module
Author: Sergey Bobkov
"""

import numpy as np

from spi_processing import center, sphere


def test_estimate():
    base_center = np.random.random(3) * 10 + 45
    base_center = np.round(base_center, decimals=1)
    base_center[2] = 0

    y_range = np.arange(100, dtype=float)
    x_range = np.arange(100, dtype=float)

    x_grid, y_grid = np.meshgrid(x_range, y_range)
    x_grid -= base_center[0]
    y_grid -= base_center[1]
    r_grid = np.sqrt(x_grid**2 + y_grid**2)

    pattern = 1 / (r_grid + 1)**2
    mask = np.zeros_like(pattern)
    mask[r_grid < 5] = 1
    pattern[mask != 0] = 0
    pattern *= 1000/pattern.max()
    pattern = center.normalise_for_estimate(pattern)
    est_center = center.estimate_center_by_rotate_symmetry(pattern, mask)

    assert (est_center == base_center).all()


def test_refine():
    base_center = np.random.random(3) * 10 + 45
    base_center = np.round(base_center, decimals=1)
    base_center[2] = 0

    shape = (100, 100)

    size = sphere.compute_size(10, 1)
    pattern = sphere.simulate_pattern(shape, base_center, size/2, 1, 1)

    rand = np.random.randint(1, 100, size=shape)
    mask = (rand > 90)
    pattern[mask != 0] = 0

    est_center = center.refine_center_by_sphere_pattern(pattern, mask, 1, 1, 1, (50, 50), size, max_shift=5, radius=40)

    assert (est_center == base_center).all()
