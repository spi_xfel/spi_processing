"""
Tests for compute_photons module
Author: Sergey Bobkov
"""

import pytest
import numpy as np

from spi_processing import cxidata, compute_photons


def test_compute_photons(tmpdir):
    filename = tmpdir.join('tmpfile.h5')
    cxidata.create_file(filename)

    image_id = cxidata.add_image_group(filename)
    image_id2 = cxidata.add_image_group(filename)

    data = np.random.randint(1, 100, size=(101, 20, 20))
    name = 'data'
    cxidata.save_dataset(filename, image_id, name, data)
    cxidata.save_dataset(filename, image_id2, name, data)
    compute_photons.compute_photons(filename, chunk_size=5)

    num_photons = np.sum(data, axis=(1, 2))
    litpixels = np.sum(data > 0, axis=(1, 2))

    assert all(num_photons == cxidata.read_dataset(filename, image_id, 'num_photons'))
    assert all(litpixels == cxidata.read_dataset(filename, image_id, 'num_litpixels'))

    with pytest.raises(ValueError, match=r"wrong number of dimensions"):
        cxidata.delete_dataset(filename, image_id, name)
        cxidata.save_dataset(filename, image_id, name, np.zeros((20, 20)))
        compute_photons.compute_photons(filename, chunk_size=5)
