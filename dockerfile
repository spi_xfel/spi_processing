FROM python:3

ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY dist/spi_processing*tar.gz .
RUN pip install spi_processing*

WORKDIR /work